# *fence-vue* CLI preset

## Instalation

1. Create your system folder and initialize GIT. You can skip this step if you
already have a system folder.
    ```shell
    $ mkdir membership
    $ cd membership
    $ git init
    ```
2. Create a Vue project using this preset. `client` is the name of the folder
inside your system folder that will contain all front end related code.
    ```shell
    $ vue create -p gitlab:gitlab.cern.ch:fence/public/vue-cli-preset-fence-vue client
    ```
    
    > :information_source: To use the `vue` command, you will need to have Vue CLI
    installed. To install it, run
    > ```shell
    > $ npm i -g @vue/cli
    > ```
    > You can also create the project without having Vue CLI installed with
    > ```shell
    > $ npx vue create -p gitlab:gitlab.cern.ch:fence/public/vue-cli-preset-fence-vue client
    > ```